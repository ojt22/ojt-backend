package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.functions.ResponseHandler;
import com.example.demo.services.Record_Service;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("record")
public class RecordController {

    @Autowired
    Record_Service record_Service;

    @GetMapping
    public ResponseHandler findAll() {
        try {
            List list = record_Service.findAll();
            return new ResponseHandler(list, 200, null);
        } catch (Exception e) {
            return new ResponseHandler(e, 400, null);
        }
    }

    @GetMapping("getByPageNumOfBabyId/{currentPage}/{pageSize}/{babyId}")
    public ResponseHandler getByPageNumOfBabyId(@PathVariable int currentPage, @PathVariable int pageSize,
            @PathVariable int babyId) {
        try {
            List list = record_Service.getByPageNumOfBabyId(currentPage, pageSize, babyId);
            return new ResponseHandler(list, 200, null);
        } catch (Exception e) {
            return new ResponseHandler(e, 400, null);
        }
    }

    @GetMapping("getPagesNum/{pageSize}/{babyId}")
    public ResponseHandler getPagesNum(@PathVariable int pageSize, @PathVariable int babyId) {
        try {
            Map list = record_Service.getPagesNum(pageSize, babyId);
            return new ResponseHandler(list, 200, null);
        } catch (Exception e) {
            return new ResponseHandler(e, 400, null);
        }
    }

}
