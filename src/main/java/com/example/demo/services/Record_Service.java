package com.example.demo.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.functions.ApiFunctions;
import com.example.demo.models.Record_Model;

@Service
public class Record_Service {

    @Autowired
    Record_Model record_Model;

    public List findAll() {
        return record_Model.findAll();
    }

    public List getRecordsById() {
        return record_Model.findAll();
    }

    // public Map getPagesNum(int pageSize) {

    // List list = record_Model.findAll();

    // int pagesSum = ApiFunctions.pageSize(pageSize, list);
    // List completedList = ApiFunctions.getNumObjectsByNumPage(pageSize, 1, list);
    // Map map = new HashMap<>();
    // map.put("SumPages", pagesSum);
    // map.put("data", completedList);

    // return map;
    // }

    public List getByPageNumOfBabyId(int pageSize, int currentPage, int id) {
        List list = record_Model.findByBabyId(id);

        List completedList = ApiFunctions.getNumObjectsByNumPage(pageSize, currentPage, list);
        return completedList;
    }

    public Map getPagesNum(int pageSize, int id) {
        List list = record_Model.findByBabyId(id);

        int pagesSum = ApiFunctions.pageSize(pageSize, list);
        List completedList = ApiFunctions.getNumObjectsByNumPage(pageSize, 1, list);
    
        Map map = new HashMap<>();
        map.put("SumPages", pagesSum);
        map.put("data", completedList);
        return map;
    }

}
