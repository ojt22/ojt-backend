package com.example.demo.models;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.jpa.repository.JpaRepository;


interface Repository_Record extends JpaRepository<Record, Integer>{
    List<Record> findBySourceUserId(int sourceUserId); 
}

@Service
public class Record_Model {
    
    @Autowired
    Repository_Record repository_Record;


    
    public List<Record> findAll(){
        return repository_Record.findAll();
    }
    

    
    public Record findById(int index){
        Record record = null;
        try{
            record = repository_Record.findById(index).get();
            return record;
        }
        catch(Exception e){
            return record;
        }
    }

    
    public List findByBabyId(int index){
        List list = null;
        try{
            list = repository_Record.findBySourceUserId(index);
            return list;
        }
        catch(Exception e){
            return list;
        }
    }


    
}
