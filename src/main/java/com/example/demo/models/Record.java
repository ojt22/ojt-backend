package com.example.demo.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "records")
public class Record{

    @Id
    int id;

    @Column(name = "source_user_id", nullable = false)
    int sourceUserId;
    
    String link;
    String upload_date;
    String record_name;
  

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getSourceUserId() {
        return sourceUserId;
    }
    public void setSourceUserId(int sourceUserId) {
        this.sourceUserId = sourceUserId;
    }
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public String getUpload_date() {
        return upload_date;
    }
    public void setUpload_date(String upload_date) {
        this.upload_date = upload_date;
    }
    public String getRecord_name() {
        return record_name;
    }
    public void setRecord_name(String record_name) {
        this.record_name = record_name;
    }

}